import React, {Component} from 'react';
import TodoService from "../services/todo.services";
import {Link} from "react-router-dom";

export default class TodoDetails extends Component{
    constructor(props) {
        super(props);
        this.state = {
            todo: {}
        }
    }

    async componentDidMount() {
        let {id} = this.props.match.params;
        let response = await TodoService.details(id);
        this.setState({todo: response.data});
    }

    async handleDelete(){
        await TodoService.delete();
        this.props.history.push('/todos');
    }

    render() {
        let {todo} = this.state;
        return <div className="container">
            <h1>Tâches : {todo.title}</h1>
            <h2>État : </h2>
            <p>{todo.completed === true ? "En cours" : "Terminée"}</p>
            <Link to={`/todos/${todo.id}/modify`} className="btn btn-secondary">Modifier</Link>
            <button className="btn btn-danger" onClick={() => this.handleDelete()}>Supprimer</button>
        </div>
    }
}
