import React, {Component} from 'react';
import UserService from "../services/users.services";
import {Link} from "react-router-dom";

export default class UsersList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    }

    async componentDidMount() {
        let users = await UserService.list();
        this.setState({users: users});
    }

    render() {
        let {users} = this.state;
        return <div className="container">
            <h1>Liste des utilisateurs : </h1>
            <div className="row">
            <table class="table">
        <thead>
          <tr>
            <th scope="col">Nom</th>
            <th scope="col">E-Mail</th>
            <th scope="col">Téléphone</th>
            <th scope="col">Nombre d'articles</th>
          </tr>
        </thead>

                {users.map(user => {
                    return         <table class="table">

                    <tbody>
                      <tr>
                        <th scope="row">{user.name}</th>
                        <td>{user.email}</td>
                        <td>{user.phone}</td>
                        <td>{user.nbrTodos}</td>
                        <Link to={`/users/${user.id}/details`} className="btn btn-primary">Détails</Link>
                      </tr>
                    </tbody>
                  </table>
                  })}
            </table>
            </div>
        </div>
    }
}
