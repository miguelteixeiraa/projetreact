import React, {Component} from 'react';
import UserService from "../services/users.services";

export default class UserDetails extends Component{

    constructor(props) {
        super(props);
        this.state = {
            user: {}
        }
    }

    async componentDidMount() {
        let {id} = this.props.match.params;
        let response = await UserService.details(id);
        this.setState({user: response.data});
    }

    async handleDelete(){
        await UserService.delete();
        this.props.history.push('/users');
    }

    render() {
        let {user} = this.state;
        return <div className="container">
            <h1>Utilisateur : {user.name}</h1>
            <h2>Contenu : </h2>
            <p>Nom d'utilisateur : {user.username}</p>
            <p> Téléphone : {user.phone}</p>
            <p> Site internet : {user.website}</p>   
            <button className="btn btn-danger" onClick={() => this.handleDelete()}>Supprimer</button>
        </div>
    }
}
