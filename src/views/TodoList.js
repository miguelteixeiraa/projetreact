import React, {Component} from 'react';
import TodoService from "../services/todo.services";
import Todo from "../components/Todo";

export default class TodoList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            todos: []
        }
    }

    async componentDidMount() {
        let todos = await TodoService.list();
        this.setState({todos: todos});
    }

    render() {
        let {todos} = this.state;
        return <div className="container">
            <h1>Liste des tâches</h1>
            <div className="row">{todos.map(todo => { return <div className="col-md-4"><Todo todo={todo}/></div> })}
            </div>
            
        </div>
    }
}
