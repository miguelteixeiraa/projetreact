import React, {Component} from 'react';

export default class Footer extends Component{

    render() {
        return <div className="card">
        <div className="card-footer">
          ToDoList React
        </div>
        <div className="card-body">
          <h5 className="card-title">CCoolLeReact Company</h5>
          <p className="card-text">Instagram : @CCoolLeReact - Téléphone : 01.12.34.56.66</p>
        </div>
      </div>
    }
}