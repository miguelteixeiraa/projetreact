import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class Todo extends Component{

    render() {
        let {todo} = this.props;
        return <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{todo.title}</h5>
                    <p className="card-text">Statut : {todo.completed === true ? 'En cours' : 'Terminée'}
                        <br/>
                        Auteur : {todo.user.name}
                    </p>
                    <Link to={`/todos/${todo.id}/details`} className="btn btn-secondary">Détails</Link>
                </div>
        </div>
    }
}
