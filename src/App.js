import React, {Component} from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import Header from "./components/Header";
import Footer from './components/Footer';
import Homepage from "./views/Homepage";
import TodoList from './views/TodoList';
import TodoAdd from './views/TodoAdd';
import TodoDetails from './views/TodoDetails';
import TodoModify from './views/TodoModify';
import UsersList from './views/UserList';
import UserDetails from './views/UserDetails';


export default class App extends Component{

    render() {
        return <BrowserRouter>

            <Header/>

            <Route exact path="/" component={Homepage} />

            <Route exact path="/todos" component={TodoList} />
            <Route exact path="/todos/add" component={TodoAdd} />
            <Route exact path="/todos/:id/details" component={TodoDetails} />
            <Route exact path="/todos/:id/modify" component={TodoModify} />

            <Route exact path="/users/" component={UsersList} />
            <Route exact path="/users/:id/details" component={UserDetails} />

            <Footer/>

        </BrowserRouter>
    }
}
